from djauth.settings import *

DATABASES = {
    'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': 'mydbtest',
       'USER': 'postgres',
       'PASSWORD': 'postgres',
       'HOST': 'postgres',
       'PORT': '5432',
   },
}
